const express = require('express');
const http = require('http');
const proxyMiddleware = require('./proxy-middleware');

const app = express();
// ENV
const port = process.env.PORT ? JSON.parse(process.env.PORT) : 8080;
// настройки прокси
const rewrites = {
    "/lk-api/": "http://lk-api-abanking.abanking-dev.ru/"
};
// настройки заглушек
const mocks = {
    '/lk-api/api/dictionary/getselectitems2': dictionaryMock
}

/**
 * Пример функции мока 
 * @param {ReqBody} req объект запроса ReqBody
 * @returns замоканный ответ в формает json
 */
function dictionaryMock(req) {
    return [
        {
            id: "-1",
            name: "1 день",
            description: "Бесплатно!!!!",
            jsonValue: -1
        }
    ]
}


for (const key in mocks) {
    app.get(key, function (req, res) {
        res.send(mocks[key](req));
    });
}

for (const key in rewrites) {
    app.use(key, proxyMiddleware(rewrites[key]));
}

server = http.createServer(app);
server.listen(port);

server.on("error", console.log);
console.log(`\x1b[33mHttp server started on:\x1b[0m \x1b[32m%s\x1b[0m`, `http://localhost:${port}`);
