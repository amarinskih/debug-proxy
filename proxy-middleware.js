const request = require('request'); // можно использовать axios вместо него

module.exports = proxyMiddleware;

function proxyMiddleware(target) {
    return function (req, res, next) {
        var url = concatPath(target, req.url);
        var resource = req.pipe(request(url));
        resource.pipe(res);
    }
}


function concatPath(target, path) {
    var targetEndWithSlash = target.endsWith('/');
    var pathStartsWithSlash = path.startsWith('/');
    if (targetEndWithSlash && pathStartsWithSlash) {
        path = path.substring(1);
    } else if (!targetEndWithSlash && !pathStartsWithSlash) {
        path = "/" + path;
    }
    return target + path;
}